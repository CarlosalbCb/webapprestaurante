const express = require('express');
const router = express.Router();

/* GET home page. */

router.get('/', (req, res) => {
    const customers = [
      {id: 1, firstName: 'John', lastName: 'Doe'},
      {id: 2, firstName: 'Brad', lastName: 'Traversy'},
      {id: 3, firstName: 'Mary', lastName: 'Swanson'},
    ];
    res.json(customers);
  });

module.exports = router;