const express = require('express');
const app = express();
const path = require('path');
const port = 5000;
const customer = require('./routes/customer');
const users = require('./routes/users');


app.use(express.static(path.join(__dirname, 'public')));
app.use('/customer', customer);
app.use('/users', users);


app.use((requ,res)=>{
  res.status(404).send('Unknown Request')
});



app.listen(port, () => `Server running on port ${port}`);
module.exports = app;
